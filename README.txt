ON THIS PAGE
------------
1) ABOUT
2) REQUIREMENTS
3) INSTALLATION
4) USE/CONFIGURATION
5) TECHNICAL OVERVIEW
6) TWIG TEMPLATE OVERVIEW


1) ABOUT
---------
MrLister allows multi-valued text fields to display
using several common list formats including:
- Natural language sentence based lists.
- CSV lists, with items enquoted or not.
- Unordered bullet point lists.
- Numerically ordered lists.
- And others.

Several text manipulation and punctuation options are also available
for modifying or standardising items in the list. These options are
especxially usefull for formatting the list to be output as natural
language sentence based lists.


2) REQUIREMENTS
----------------
MrLister requires the latest release of Drupal 8.


3) INSTALLATION
-----------------
1. Include the module in your /modules/contrib/
   directory. If using composer this can be done
   with the terminal command:

   composer require drupal/mrlister

2. Enable the module via the Drupal administration
   interface. If using drush this can be done with
   the terminal command:

   drush pm-enable mrlister -y


4) USE/CONFIGURATION
--------------------
1. Select the "Manage Display" tab of any content
   type that contains a "Multi-valued Text Field".

2. Scroll to the field and select the "Format"
   columns "Mr Lister" option.

3. Click the "Gear Icon" to customize the list
   display to your desired format.

4. Click both "Update" then "Save" (at page bottom)
   to save your changes.

5. Review your resulting updates by visiting a node
   display that contains multiple values for the
   field. Note that node displays are often cached
   so perform any necessary cache clearing required.
   Often this can be as simple as clicking "Edit"
   then "Save" on the particular node display.


5) TECHNICAL OVERVIEW
---------------------
* Review the MrLister module's build status at
  https://travis-ci.org/drupal-media/crop

* Scrutinize the MrLister module's code quality at
  https://scrutinizer-ci.com/g/drupal-media/crop/


6) TWIG TEMPLATE OVERVIEW
--------------------------
See "mrlister/templates/mrlister-field.html.twig"
for additional details regarding html strucutres
and css convenience classes at play.
