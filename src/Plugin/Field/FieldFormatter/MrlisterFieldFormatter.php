<?php

namespace Drupal\mrlister\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'mrlister_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "mrlister_field_formatter",
 *   label = @Translation("Mr Lister"),
 *   field_types = {
 *     "string"
 *   }
 * )
 * )
 */
class MrlisterFieldFormatter extends FormatterBase {

  /**
   * Get select options for mrlister_field_formatter form items.
   *
   * @return array
   *   An array of select option display text keyed by the machine id value.
   */
  public static function getOptions($type) {
    $options = [];
    switch ($type) {
      case 'mrl_wrap_and_item_type':
        $options = [
          "ul_li" => "UL > LI",
          "ol_li" => "OL > LI",
          "div_p" => "DIV > P",
          "div_div" => "DIV > DIV",
          "div_span" => "DIV > SPAN",
          "div_none" => "DIV > NONE",
          "p_span" => "P > SPAN",
          "p_none" => "P > NONE",
          "span_span" => "SPAN > SPAN",
          "span_none" => "SPAN > NONE",
        ];
        break;

      case 'mrl_modifier_type':
        $options = [
          "none" => t("No modifications."),
          "capital" => t("Capitalize first character of first word."),
          "title" => t("Capitalize first character of all words."),
          "lower" => t("Lower case all characters of all words."),
        ];
        break;

      case 'mrl_punctuation_type':
        // The mrl_punctuation_types names with "_default" appended indicate
        // that the punctuation will be applied only when existing punctuation
        // is not already present on the user input. Where as the types named
        // without "_default" appended, indkcate that they are "enforced" so
        // the punctuation will be appended to the user input value regardless
        // of any punctuation the user input may already have.
        $options = [
          "none" => t("No punctuation changes."),
          "remove" => t("Remove trailing punctuation '.', '?', and '!'."),
          "period" => t("Enforce periods."),
          "period_default" => t("Default to periods."),
          "question" => t("Enforce question marks."),
          "question_default" => t("Default to question marks."),
          "exclamation" => t("Enforce exclamation points."),
          "exclamation_default" => t("Default to exclamation points."),
        ];
        break;

      case 'mrl_csv_type':
        $options = [
          "none" => "No csv treatment.",
          "csv" => "Apply csv.",
          "quote" => "Apply quotes.",
          "csv_quote" => "Apply csv and quotes.",
          "csv_and_period" => "Apply csv, intro last item with 'and', append a period.",
          "csv_quote_and_period" => "Apply csv and quotes, intro last item with 'and', append a period.",
        ];
        break;

      case 'mrl_label_type':
        // These options are not selectable form options like the others.
        // Instead they hold the "label text" for our settings form inputs
        // and our settings form summary.
        $options = [
          "mrl_intro_text" => t("Intro text"),
          "mrl_wrap_class" => t("Wrap class"),
          "mrl_wrap_and_item_type" => t('Wrap and Items tags'),
          "mrl_modifier_type" => t("Item modification"),
          "mrl_punctuation_type" => t("Item punctuation"),
          "mrl_csv_type" => t("Item csv treatment"),
        ];
        break;
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'mrl_intro_text' => '',
      'mrl_wrap_class' => '',
      'mrl_wrap_and_item_type' => 'div_div',
      'mrl_modifier_type' => 'none',
      'mrl_punctuation_type' => 'none',
      'mrl_csv_type' => 'none',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'mrl_intro_text' => [
        '#type' => 'textfield',
        '#title' => $this->getOptions('mrl_label_type')['mrl_intro_text'],
        '#default_value' => $this->getSetting('mrl_intro_text'),
      ],
      'mrl_wrap_class' => [
        '#type' => 'textfield',
        '#title' => $this->getOptions('mrl_label_type')['mrl_wrap_class'],
        '#default_value' => $this->getSetting('mrl_wrap_class'),
        '#description' => $this->t('Seperate multiple with a blank space.'),
        // https://drupal.stackexchange.com/questions/141172/how-to-use-element-validate-and-value-callback-in-drupal-8
        // '#element_validate' => [ [$this, 'cleanCssClasses'] ],.
      ],
      'mrl_wrap_and_item_type' => [
        '#type' => 'select',
        '#title' => $this->getOptions('mrl_label_type')['mrl_wrap_and_item_type'],
        '#options' => $this->getOptions('mrl_wrap_and_item_type'),
        '#default_value' => $this->getSetting('mrl_wrap_and_item_type'),
      ],
      'mrl_modifier_type' => [
        '#type' => 'select',
        '#title' => $this->getOptions('mrl_label_type')['mrl_modifier_type'],
        '#options' => $this->getOptions('mrl_modifier_type'),
        '#default_value' => $this->getSetting('mrl_modifier_type'),
        '#description' => $this->t('Modifications do exactly what they say and nothing more.'),
      ],
      'mrl_punctuation_type' => [
        '#type' => 'select',
        '#title' => $this->getOptions('mrl_label_type')['mrl_punctuation_type'],
        '#options' => $this->getOptions('mrl_punctuation_type'),
        '#default_value' => $this->getSetting('mrl_punctuation_type'),
        '#description' => $this->t('"Default" means apply only if no punctuation is present.'),
      ],
      'mrl_csv_type' => [
        '#type' => 'select',
        '#title' => $this->getOptions('mrl_label_type')['mrl_csv_type'],
        '#options' => $this->getOptions('mrl_csv_type'),
        '#default_value' => $this->getSetting('mrl_csv_type'),
      ],
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = [
      $this->getOptions('mrl_label_type')['mrl_intro_text'] . ": " . (empty($settings['mrl_intro_text']) ? 'None' : $settings['mrl_intro_text']),
      $this->getOptions('mrl_label_type')['mrl_wrap_class'] . ": " . (empty($settings['mrl_wrap_class']) ? 'None' : $settings['mrl_wrap_class']),
      $this->getOptions('mrl_label_type')['mrl_wrap_and_item_type'] . ": " . $this->getOptions('mrl_wrap_and_item_type')[$settings['mrl_wrap_and_item_type']],
      $this->getOptions('mrl_label_type')['mrl_modifier_type'] . ": " . $this->getOptions('mrl_modifier_type')[$settings['mrl_modifier_type']],
      $this->getOptions('mrl_label_type')['mrl_punctuation_type'] . ": " . $this->getOptions('mrl_punctuation_type')[$settings['mrl_punctuation_type']],
      $this->getOptions('mrl_label_type')['mrl_csv_type'] . ": " . $this->getOptions('mrl_csv_type')[$settings['mrl_csv_type']],
    ];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    // Break down "mrl_wrap_and_item_type" into its parts.
    $mrl_wrap_type = explode("_", $this->settings['mrl_wrap_and_item_type'])[0];
    $mrl_item_type = explode("_", $this->settings['mrl_wrap_and_item_type'])[1];

    // For the "intro" element use the most semantic html structure
    // possible as determined by the type of list structure we intro.
    $mrl_intro_type = ($mrl_item_type == 'span' || $mrl_item_type == 'none' ) ? 'span' : 'div';
    $mrl_intro_place = ($mrl_item_type == 'li') ? 'outside_wrap' : 'inside_wrap';

    // Create the parent render element for the "mrlister-field.html.twig".
    $element = [];
    $element['#theme'] = 'mrlister_field';

    // Later "mrlister_preprocess_mrlister_field()" copies these to the
    // template's root ['variables'] array for "mrlister-field.html.twig".
    // There may be a better way but for now this works fine.
    $element['#mrl_variables']['mrl_intro_place'] = $mrl_intro_place;
    $element['#mrl_variables']['mrl_intro_text'] = $this->settings['mrl_intro_text'];
    $element['#mrl_variables']['mrl_intro_type'] = $mrl_intro_type;
    $element['#mrl_variables']['mrl_wrap_type'] = $mrl_wrap_type;
    $element['#mrl_variables']['mrl_item_type'] = $mrl_item_type;

    // Add classes to the wrap element.
    $element['#attributes']['class'][] = 'mrlister-wrap';
    $element['#attributes']['class'][] = 'mrlister-items-' . count($items);
    $element['#attributes']['class'] = array_merge($element['#attributes']['class'], $this->prepCssClasses($this->getSetting('mrl_wrap_class')));

    // Add each item as a simple child #markup to the render array.
    // Per the "FieldItemListInterface" each item must be added using
    // a numeric delta that enumerates from zero up.
    foreach ($items as $delta => $item) {
      $element[$delta]['#markup'] = $this->viewValue($item, ($delta + 1 == count($items)));
    }
    return $element;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   * @param bool $is_last_delta
   *   Whether or not this element is the last among siblings.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item, $is_last_delta) {
    // "String" field types have no  text format assigned to it,
    // so the user input should equal the output but for newlines.
    $value = trim(nl2br(Html::escape($item->value)));

    // Determine if the value already contains punctuation.
    $punctuated = in_array(substr($value, -1), ['.', '!', '?']);

    // For the "Enforced" mrl_punctuation_types remove any existing punctuation.
    switch ($this->settings['mrl_punctuation_type']) {
      case 'remove':
      case 'period':
      case 'question':
      case 'exclamation':
        $value = $punctuated ? substr($value, 0, -1) : $value;
        break;
    }

    // Now we can punctuate according to the mrl_punctuation_type selected.
    switch ($this->settings['mrl_punctuation_type']) {
      case 'period':
        $value .= ".";
        break;

      case 'question':
        $value .= "?";
        break;

      case 'exclamation':
        $value .= "!";
        break;

      // "Default" punctuations apply only if no other punctuation is present.
      case 'period_default':
        $value = $punctuated ? $value : $value . ".";
        break;

      case 'question_default':
        $value = $punctuated ? $value : $value . "?";
        break;

      case 'exclamation_default':
        $value = $punctuated ? $value : $value . "!";
        break;
    }

    // Modify the value according to the mrl_modifier_type selected.
    switch ($this->settings['mrl_modifier_type']) {
      case 'none':
        break;

      case 'capital':
        $value = ucfirst($value);
        break;

      case 'title':
        $value = ucwords($value);
        break;

      case 'lower':
        $value = strtolower($value);
        break;
    }

    // Apply a csv treatment according to the mrl_csv_type selected.
    switch ($this->settings['mrl_csv_type']) {
      case 'none':
        break;

      case 'csv':
        $value = $is_last_delta ? $value : ($value . ', ');
        break;

      case 'quote':
        // Apply quotes with no trailing space on the last item.
        $value = $is_last_delta ? ('"' . $value . '"') : ('"' . $value . '" ');
        break;

      case 'csv_quote':
        // Apply quotes and commas with no comma or trailing space on the last.
        $value = $is_last_delta ? ('"' . $value . '"') : ('"' . $value . '", ');
        break;

      case 'csv_and_period':
        // Punctuate with a period if no other punctuation is present.
        $value = $punctuated ? $value : $value . ".";
        // Apply commas but for the last item which gets "and" prepended.
        $value = $is_last_delta ? (' ' . $this->t('and') . ' ' . $value) : ($value . ', ');
        break;

      case 'csv_quote_and_period':
        // Apply quotes and commas but for the last which gets "and" prepended.
        $value = $is_last_delta ? ($this->t('and') . ' "' . $value . '".') : ('"' . $value . '", ');
        break;
    }
    return $value;
  }

  /**
   * Clean a css classes string and return as an array of classes.
   *
   * @param string $str
   *   A string containing css classes delimited by spaces.
   *
   * @return array
   *   An array of valid css class names.
   */
  private function prepCssClasses($str) {
    $cleaned = [];
    foreach (explode(' ', trim($str)) as $class) {
      $cleaned[] = Html::cleanCssIdentifier($class);
    }
    return $cleaned;
  }

}
